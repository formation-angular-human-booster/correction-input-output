import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComposantEnfantComponent } from './components/composant-enfant/composant-enfant.component';
import { ComposantParentComponent } from './components/composant-parent/composant-parent.component';
import { GrandParentComponent } from './components/grand-parent/grand-parent.component';

@NgModule({
  declarations: [
    AppComponent,
    ComposantEnfantComponent,
    ComposantParentComponent,
    GrandParentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
