import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-composant-parent',
  templateUrl: './composant-parent.component.html',
  styleUrls: ['./composant-parent.component.css']
})
export class ComposantParentComponent implements OnInit {
  @Input() numeroParent;
  nbClickEnfant1 = 0;
  nbClickEnfant2 = 0;
  nbClickEnfant3 = 0;
  constructor() { }

  ngOnInit() {
  }

  addOneClick($event) {
    if ( $event === 1 ) {
      this.nbClickEnfant1 += 1;
    }
    if ( $event === 2 ) {
      this.nbClickEnfant2 += 1;
    }
    if ( $event === 3 ) {
      this.nbClickEnfant3 += 1;
    }
  }
}
