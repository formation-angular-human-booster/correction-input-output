import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-composant-enfant',
  templateUrl: './composant-enfant.component.html',
  styleUrls: ['./composant-enfant.component.css']
})
export class ComposantEnfantComponent implements OnInit {
  @Input() numComposant;
  @Input() numeroParent;
  @Output() clickEmitter: EventEmitter<number> = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
  }

  someOneClick() {
    this.clickEmitter.emit(this.numComposant);
  }
}
